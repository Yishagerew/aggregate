import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
/**
* Servlet implementation class HelloWorld
*/
@WebServlet("/salutation/*")
public class HelloWorld extends HttpServlet {
/**
*
*/
	private static final Map<String,String>greetings;
	static
	{
 final Map<String,String>map=new HashMap<String,String>();
 map.put("en", "Hi all");
 map.put("it", "Comestai");
 map.put("Am", "selam new");
 greetings=Collections.unmodifiableMap(map);
	}
private static final long serialVersionUID = 3376725678887362349L;
/**
* @see HttpServlet#HttpServlet()
*/
public HelloWorld() {
super();
// TODO Auto-generated constructor stub
}
/**
* @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
*/
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
String pathinfo=request.getPathInfo();
System.out.println(pathinfo);
String indexParam=request.getParameter("format");
String index=indexParam==null?null:new String(indexParam);
System.out.println("Language index is"+index);
//String pathinfo="/en";
String languageCode=pathinfo.substring(1);
System.out.println(languageCode);
if("json".equals(index))
{
	JsonReplay(response,languageCode);
}
else if("xml".equals(index))
{
try {
	XMlreplay(response,languageCode);
} catch (ParserConfigurationException | TransformerFactoryConfigurationError
		| TransformerException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}	
}
else if("csv".equals(index))
{
	CSVreplay(response,languageCode);
}
else
{
	htmlreplay(response,languageCode);
}

}
@SuppressWarnings("rawtypes")
private void htmlreplay(HttpServletResponse response, String index) throws IOException {

	String html = "<html><head><title>Language Greetings</title></head><body><ol>";
	
	if (index==null) {
		Iterator iter=greetings.entrySet().iterator();
		while(iter.hasNext())
		{
			Map.Entry entry = (Map.Entry) iter.next();
			html = html + "<li>"+entry.getValue()+"</li>";
		}			
	} else {
		String greetingname = greetings.get(index);
		html = html + "<li>"+greetingname+"</li>";
	}
	html = html + "</ol></body></html>";
	response.setContentType("text/html");
	response.getWriter().write(html);
}
@SuppressWarnings("rawtypes")
private void CSVreplay(HttpServletResponse response, String languageCode) {

	response.setContentType("text/csv");
	System.out.println("Greeting,type\t\n");
	String output="";
	
	if(languageCode==null)
	{
		Iterator iter=greetings.entrySet().iterator();
		while(iter.hasNext())
		{
			Map.Entry entry = (Map.Entry) iter.next();
			output=output+"Greeting\t"+entry.getValue()+"\n";
		}
	}
	else
		output=output+"Greeting\n"+greetings.get(languageCode);
	try {
		response.getWriter().write(output);
	} catch (IOException e) {
System.out.println(e.getMessage());
		e.printStackTrace();
	}
}
private void XMlreplay(HttpServletResponse response, String languageCode) throws ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException, IOException {

	response.setContentType("text/xml");
	DocumentBuilder builder=DocumentBuilderFactory.newInstance().newDocumentBuilder();
	Document doc=builder.newDocument();
	Element root=doc.createElement("Languages");
	doc.appendChild(root);
	Element listoflanguages=doc.createElement("Greetings");
	if(languageCode==null)
	{
		for(Map.Entry<String, String>entry:greetings.entrySet())
		{
			Element greetinglanguage=doc.createElement("Greeting");
			greetinglanguage.setTextContent(entry.getValue());
			listoflanguages.appendChild(greetinglanguage);
		}
	}
	else
	{
		Element greetinglanguage=doc.createElement("Greeting");
		greetinglanguage.setTextContent(greetings.get(languageCode));
		listoflanguages.appendChild(greetinglanguage);
	}
	root.appendChild(listoflanguages);
	Transformer transform=TransformerFactory.newInstance().newTransformer();
	StringWriter writer=new StringWriter();
	StreamResult result=new StreamResult(writer);
	DOMSource source=new DOMSource(doc);//set as a node
	transform.transform(source, result);
	String stringfinal=writer.toString();
	response.getWriter().write(stringfinal);
}
@SuppressWarnings("unchecked")
private void JsonReplay(HttpServletResponse response, String index) throws IOException {
	response.setContentType("text/json");
JSONObject obj=new JSONObject();
JSONArray jArray=new JSONArray();
int count=0;
if(index==null)
{
	for(Object key:greetings.keySet())
	{
		jArray.put(count, greetings.get(key));
		count++;
	}
}
else
{
	count++;
	jArray.put(count, greetings.get(index));
}
obj.put("Greetings", jArray);
StringWriter writer=new StringWriter();
JSONValue.writeJSONString(obj, writer);
response.getWriter().write(obj.toString());;
}
/**
* @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
*/
}